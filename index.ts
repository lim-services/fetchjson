import axios from 'axios';

const url  = 'http://jsonplaceholder.typicode.com/todos/1';

// interface defines the structure of the object
// this interface uses 3 properties
interface Todo {
  id: number;
  title: string;
  completed: boolean;
};

//Grab the whole JSON Data index 0
axios.get(url).then((response) => {
  console.log(response.data);
});

// Prints out a much clearer JSON Data
axios.get(url).then((response) => {
  const todo = response.data as Todo;

  const id = todo.id;
  const title = todo.title;
  const completed = todo.completed;

  logTodo(id, title, completed);
});

// helper arrow function
// should declare argument type
const logTodo = (id: number, title: string, completed: boolean) => {
  console.log(`
  The Todo with ID: ${id}
  Has a title of: ${title}
  Is it finished? ${completed}
`)
}